﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Windows;

namespace wpf_test
{
    public class LocalizationResourceManager : INotifyPropertyChanged
    {
        static LocalizationResourceManager m_instance;
        public event PropertyChangedEventHandler PropertyChanged;

        public static LocalizationResourceManager Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new LocalizationResourceManager();
                }

                return m_instance;
            }
        }

        public string this[string text]
        {
            get
            {
                return Languages.Resource.ResourceManager.GetString(text);
            }
        }

        public void SetCulture(CultureInfo lang)
        {
            Thread.CurrentThread.CurrentUICulture = lang;
            Thread.CurrentThread.CurrentCulture = lang;

            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(null));
        }
    }
}
