﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Xaml;

namespace wpf_test
{
    public class TranslateExtension : Binding
    {
        public TranslateExtension(string name) :
            base(string.Format("[{0}]", name))
        {
            Mode = BindingMode.OneWay;
            Source = LocalizationResourceManager.Instance;
        }
    }
}
